# Maven dependency editor

* A CLI for search maven repository.
* A CLI for update pom.xml to add dependency.

## TODO

* [x] parse pom.xml
* [x] traverse local repository
* [x] search by groupId/artifactId/description
* [ ] query [search.maven.org](https://search.maven.org)
* [ ] interactive selection
* [ ] update pom.xml

## LICENSE

This software is released under the MIT License, see LICENSE.txt.
