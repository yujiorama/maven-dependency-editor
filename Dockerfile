FROM ruby:2.7-rc-slim-buster

WORKDIR /app

COPY Gemfile ./

RUN bundle config set without 'local' \
 && bundle install --path=vendor/bundle

COPY Rakefile ./
COPY lib/ ./lib/
COPY spec/ ./spec/
COPY testdata/ ./testdata/

ENTRYPOINT ["bundle", "exec"]
CMD ["rake", "-Ilib", "spec"]
