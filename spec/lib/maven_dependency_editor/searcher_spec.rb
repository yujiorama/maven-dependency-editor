require 'spec_helper'
require 'maven_dependency_editor'
require 'rexml/document'

describe "about search local repository" do
  context 'with gruopId:"jakarta"' do
    subject(:actual) {
      query = MavenDependencyEditor::Searcher.query({groupId: 'jakarta'})
      MavenDependencyEditor::Searcher.search(mode: :local, query: query, format: :maven, profile: false)
    }

    it 'then must return hash' do
      expect(actual).to_not be_nil
    end

    it 'then must return pom list' do
      expect(actual[:results]).to_not be_nil
      expect(actual[:results].size).to eq 4
    end

    it 'then must return jakarta.activation-api:1.2.1 of pom[0]' do
      pom = ::REXML::Document.new(actual[:results][0])
      expect(pom).to_not be_nil
      pom.elements.each('dependecy/groupId') do |elm|
        expect(elm.text).to eq('jakarta')
      end
      pom.elements.each('dependecy/artifactId') do |elm|
        expect(elm.text).to eq('activation-api')
      end
      pom.elements.each('dependecy/version') do |elm|
        expect(elm.text).to eq('1.2.1')
      end
    end

    it 'then must return jakarta:annotation-api:1.3.4 of pom[1]' do
      pom = ::REXML::Document.new(actual[:results][2])
      expect(pom).to_not be_nil
      pom.elements.each('dependecy/groupId') do |elm|
        expect(elm.text).to eq('jakarta')
      end
      pom.elements.each('dependecy/artifactId') do |elm|
        expect(elm.text).to eq('annotation-api')
      end
      pom.elements.each('dependecy/version') do |elm|
        expect(elm.text).to eq('1.3.4')
      end
    end

    it 'then must return jakarta:validator-api:2.0.1 of pom[2]' do
      pom = ::REXML::Document.new(actual[:results][2])
      expect(pom).to_not be_nil
      pom.elements.each('dependecy/groupId') do |elm|
        expect(elm.text).to eq('jakarta')
      end
      pom.elements.each('dependecy/artifactId') do |elm|
        expect(elm.text).to eq('validator-api')
      end
      pom.elements.each('dependecy/version') do |elm|
        expect(elm.text).to eq('2.0.1')
      end
    end

    it 'then must return jakarta.xml.bind:jakarta.xml.bind-api:2.3.2 of pom[3]' do
      pom = ::REXML::Document.new(actual[:results][3])
      expect(pom).to_not be_nil
      pom.elements.each('dependecy/groupId') do |elm|
        expect(elm.text).to eq('jakarta.xml.bind')
      end
      pom.elements.each('dependecy/artifactId') do |elm|
        expect(elm.text).to eq('jakarta.xml.bind-api')
      end
      pom.elements.each('dependecy/version') do |elm|
        expect(elm.text).to eq('2.3.2')
      end
    end
  end

  context 'with keyword:"validation"' do
    subject(:actual) {
      query = MavenDependencyEditor::Searcher.query({keyword: 'validation'})
      MavenDependencyEditor::Searcher.search(mode: :local, query: query, format: :maven, profile: false)
    }

    it 'then must return hash' do
      expect(actual).to_not be_nil
    end

    it 'then must return pom list' do
      expect(actual[:results]).to_not be_nil
      expect(actual[:results].size).to eq 1
    end

    it 'then must return jakarta.validation:validator-api:2.0.1 of pom[0]' do
      pom = ::REXML::Document.new(actual[:results][0])
      expect(pom).to_not be_nil
      pom.elements.each('dependecy/groupId') do |elm|
        expect(elm.text).to eq('jakarta.validation')
      end
      pom.elements.each('dependecy/artifactId') do |elm|
        expect(elm.text).to eq('validator-api')
      end
      pom.elements.each('dependecy/version') do |elm|
        expect(elm.text).to eq('2.0.1')
      end
    end
  end

  context 'with gradle like dependency string:"jakarta.annotation:annotation-api:.*"' do
    subject(:actual) {
      query = MavenDependencyEditor::Searcher.queryOf('jakarta.annotation:annotation-api:.*')
      MavenDependencyEditor::Searcher.search(mode: :local, query: query, format: :maven, profile: false)
    }

    it 'then must return hash' do
      expect(actual).to_not be_nil
    end

    it 'then must return pom list' do
      expect(actual[:results]).to_not be_nil
      expect(actual[:results].size).to eq 1
    end

    it 'then must return jakarta:annotation-api:1.3.4 of pom[0]' do
      pom = ::REXML::Document.new(actual[:results][0])
      expect(pom).to_not be_nil
      pom.elements.each('dependecy/groupId') do |elm|
        expect(elm.text).to eq('jakarta.annotation')
      end
      pom.elements.each('dependecy/artifactId') do |elm|
        expect(elm.text).to eq('annotation-api')
      end
      pom.elements.each('dependecy/version') do |elm|
        expect(elm.text).to eq('1.3.4')
      end
    end
  end

  context 'with keyword:"not found"' do
    subject(:actual) {
      query = MavenDependencyEditor::Searcher.query({keyword: 'not found'})
      MavenDependencyEditor::Searcher.search(mode: :local, query: query, format: :maven, profile: false)
    }

    it 'then must return hash' do
      expect(actual).to_not be_nil
    end

    it 'then must return empty pom list' do
      expect(actual[:results]).to_not be_nil
      expect(actual[:results].size).to eq 0
    end
  end
end

describe "about search remote repository" do
  context 'with keyword:"jakarta.platform"' do
    subject(:actual) {
      query = MavenDependencyEditor::Searcher.query({groupId: 'jakarta.platform'})
      MavenDependencyEditor::Searcher.search(mode: :remote, query: query, format: :maven, profile: false)
    }

    it 'then must return hash' do
      expect(actual).to_not be_nil
    end

    it 'then must return pom list' do
      expect(actual[:results]).to_not be_nil
      expect(actual[:results].size).to eq 4
    end

    it 'then must return jakarta.platform.jakartaee-api-parent:8.0.0 of pom[0]' do
      pom = ::REXML::Document.new(actual[:results][0])
      expect(pom).to_not be_nil
      pom.elements.each('dependecy/groupId') do |elm|
        expect(elm.text).to eq('jakarta.platform')
      end
      pom.elements.each('dependecy/artifactId') do |elm|
        expect(elm.text).to eq('jakartaee-api-parent')
      end
      pom.elements.each('dependecy/version') do |elm|
        expect(elm.text).to eq('8.0.0')
      end
    end

    it 'then must return jakarta.platform.jakarta.jakartaee-bom:8.0.0 of pom[1]' do
      pom = ::REXML::Document.new(actual[:results][2])
      expect(pom).to_not be_nil
      pom.elements.each('dependecy/groupId') do |elm|
        expect(elm.text).to eq('jakarta.platform')
      end
      pom.elements.each('dependecy/artifactId') do |elm|
        expect(elm.text).to eq('jakarta.jakartaee-bom')
      end
      pom.elements.each('dependecy/version') do |elm|
        expect(elm.text).to eq('8.0.0')
      end
    end

    it 'then must return jakarta.platform.jakarta.jakartaee-web-api:8.0.0 of pom[2]' do
      pom = ::REXML::Document.new(actual[:results][2])
      expect(pom).to_not be_nil
      pom.elements.each('dependecy/groupId') do |elm|
        expect(elm.text).to eq('jakarta.platform')
      end
      pom.elements.each('dependecy/artifactId') do |elm|
        expect(elm.text).to eq('jakarta.jakartaee-web-api')
      end
      pom.elements.each('dependecy/version') do |elm|
        expect(elm.text).to eq('8.0.0')
      end
    end

    it 'then must return jakarta.platform.jakartaee-api:8.0.0 of pom[3]' do
      pom = ::REXML::Document.new(actual[:results][3])
      expect(pom).to_not be_nil
      pom.elements.each('dependecy/groupId') do |elm|
        expect(elm.text).to eq('jakarta.platform')
      end
      pom.elements.each('dependecy/artifactId') do |elm|
        expect(elm.text).to eq('jakartaee-api')
      end
      pom.elements.each('dependecy/version') do |elm|
        expect(elm.text).to eq('8.0.0')
      end
    end
  end

  context 'with gradle like dependency string:"jakarta.platform.jakartaee-api:.*"' do
    subject(:actual) {
      query = MavenDependencyEditor::Searcher.queryOf('jakarta.platform.jakartaee-api:.*')
      MavenDependencyEditor::Searcher.search(mode: :remote, query: query, format: :maven, profile: false)
    }

    it 'then must return hash' do
      expect(actual).to_not be_nil
    end

    it 'then must return pom list' do
      expect(actual[:results]).to_not be_nil
      expect(actual[:results].size).to eq 1
    end

    it 'then must return jakarta:annotation-api:1.3.5 of pom[0]' do
      pom = ::REXML::Document.new(actual[:results][0])
      expect(pom).to_not be_nil
      pom.elements.each('dependecy/groupId') do |elm|
        expect(elm.text).to eq('jakarta.annotation')
      end
      pom.elements.each('dependecy/artifactId') do |elm|
        expect(elm.text).to eq('annotation-api')
      end
      pom.elements.each('dependecy/version') do |elm|
        expect(elm.text).to eq('1.3.5')
      end
    end
  end

  context 'with keyword:"myst AND tree"' do
    subject(:actual) {
      query = MavenDependencyEditor::Searcher.query({keyword: 'myst AND tree'})
      MavenDependencyEditor::Searcher.search(mode: :remote, query: query, format: :maven, profile: false)
    }

    it 'then must return hash' do
      expect(actual).to_not be_nil
    end

    it 'then must return empty pom list' do
      expect(actual[:results]).to_not be_nil
      expect(actual[:results].size).to eq 0
    end
  end
end
