require 'maven_dependency_editor/searcher/query'
require 'maven_dependency_editor/searcher/local'
require 'maven_dependency_editor/searcher/remote'

module MavenDependencyEditor
  module Searcher

    def self.query(h)
      return Query.new(h)
    end

    def self.queryOf(s)
      return Query.of(s)
    end

    def self.search(mode: :local, query:, format: :maven, profile: false)
      case mode
      when :local
        return Local.search(query: query, format: format, profile: profile)
      when :remote
        return Remote.search(query: query, format: format, profile: profile)
      end
    end
  end
end
