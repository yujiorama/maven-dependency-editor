usage       'search [options]'
summary     'search maven repository'
description <<~DESC
  search maven repository
DESC

flag :h, :help, 'show help for this command' do |v, cmd|
  raise cmd.help
end

flag :l, :local,  'search local repository'
flag :r, :remote, 'search remote repository'
flag :c, :csv,    'output as csv'
option :f, :format, 'render format [maven or gradle or json or yaml]',
  argument: :required,
  transform: -> (v) { v.to_sym }
option :q, :gradlestring, 'gradle like dependency notation query(exclusive with a,g,v,k)',
  argument: :required,
  transform: -> (v) { MavenDependencyEditor::Searcher.queryOf(v) }
option :a, :query_artifactId, 'query [artifactId] (exclusive with q)',
  argument: :required,
  transform: -> (v) { { artifactId: v } }
option :g, :query_groupId, 'query [groupId] (exclusive with q)',
  argument: :required,
  transform: -> (v) { { groupId: v } }
option :v, :query_version, 'query [keyword] (exclusive with q)',
  argument: :required,
  transform: -> (v) { { version: v } }
option :k, :query_keyword, 'query [keyword] (exclusive with q)',
  argument: :required,
  transform: -> (v) { { keyword: v } }

run do |opts, argv, cmd|

  q = opts[:gradlestring]
  unless q
    h = cmd.option_definitions
      .select     { |v| ! opts[v.long.to_sym].nil? }
      .select     { |v| v.long.start_with?('query_') }
      .inject({}) { |h, v| h.merge(opts[v.long.to_sym]) }
    raise cmd.help if h.empty?

    q = MavenDependencyEditor::Searcher.query(h)
  end

  format = opts[:csv] ? :yaml : (opts[:format] || :maven)

  local_result = if opts[:local]
    MavenDependencyEditor::Searcher.search(mode: :local, query: q, format: format)
  else
    {results: []}
  end

  remote_result = if opts[:remote]
    MavenDependencyEditor::Searcher.search(mode: :remote, query: q, format: format)
  else
    {results: []}
  end

  result = local_result[:results].union(remote_result[:results]).uniq

  if opts[:csv]

    require 'csv'
    require 'yaml'

    xs = result
      .map     { |v| ::YAML.load(v, fallback:{}, symbolize_names:true) }
      .sort_by { |v| [ v.groupId, v.artifactId, v.version ] }
      .map     { |v| [ v.groupId, v.artifactId, v.version ] }

    csv = ::CSV.generate(headers:true, write_headers:true, col_sep:',', row_sep: :auto) do |csv|
      csv.add_row(%w[groupId artifactId version])
      xs.each { |v| csv.add_row(v) }
    end
    puts csv

  else

    result.sort.each do |doc|
      puts doc
    end

  end
end
