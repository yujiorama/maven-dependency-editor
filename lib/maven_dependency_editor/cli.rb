require 'cri'

module MavenDependencyEditor
  class Cli

    def self.invoke
      begin
        cli = Cli.new
        cli.run(ARGV)
      rescue ::Cri::Command::CriExitException => e

        exit(1) if e.error?

      rescue => e

        puts e.message
        exit(1)

      end

      exit(0)
    end

    def run(argv)
      command.run(argv)
    end

    attr_reader :command

    def initialize
      @command = ::Cri::Command.define do
        name        'maven-dependency-editor'
        usage       'maven-dependency-editor [options] [command] [options]'
        summary     'summary(TODO)'
        description <<~DESC
          Maven Depenency Editor
        DESC

        flag :h, :help, 'show help for this command' do |v, cmd|
          raise cmd.help
        end
      end

      command.add_command ::Cri::Command.new_basic_help

      ::Dir.glob(::File.join(::File.dirname(__FILE__), 'cli', '*.rb')) do |filename|
        command.add_command ::Cri::Command.load_file(filename, infer_name: true)
      end
    end
  end
end
