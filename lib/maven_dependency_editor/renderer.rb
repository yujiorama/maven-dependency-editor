require 'maven_dependency_editor/model'
require 'erb'
require 'yaml'

module MavenDependencyEditor

  class Renderer

    @@xml_template = ::ERB.new(DEPENDENCY_ELEMENT_XML_TEMPLATE)

    def self.xml_template
      @@xml_template
    end

    attr_accessor :groupId, :artifactId, :description, :version, :meta, :format

    def initialize(h, format)
      @groupId = (h[:groupId] || '').strip
      @artifactId = (h[:artifactId] || '').strip
      @description = (h[:description] || '').strip
      @version = (h[:version] || '').strip
      @meta = h[:meta]
      @format = format
    end

    def to_s
      case format
      when :maven
        to_xml
      when :yaml
        to_yaml
      when :json
        to_json
      end
    end

    def to_xml
        self.class.xml_template.result(self.instance_eval { binding })
    end

    def to_yaml
        ::YAML.dump(self)
    end

    def to_json
        ::YAML.to_json(self)
    end
  end
end
