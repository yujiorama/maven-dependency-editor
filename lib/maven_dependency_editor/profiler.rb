require 'time'

module MavenDependencyEditor

  class StopWatch

    def start(&b)
      @start = ::Time.now
      result = b.call self
      stop
      result
    end

    def stop
      @end = ::Time.now unless @end
    end

    def interval
      return 0 unless @start
      return 0 unless @end
      return 0 if @end < @start
      return @end - @start
    end
  end

  class Profiler

    def initialize(enable)
      @enable = enable
      @store = {}
    end

    def start(label, &b)
      if @enable
        stopwatch = StopWatch.new
        result = stopwatch.start { |v| b.call v }
        stopwatch.stop
        @store[label] = stopwatch.interval
        result
      else
        b.call
      end
    end

    def [](label)
      if @enable
        @store[:total] = @store.values.inject(0) { |total, i| total + i }
        @store[label] || 0
      else
        0
      end
    end
  end
end
