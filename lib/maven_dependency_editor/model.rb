module MavenDependencyEditor

  DEPENDENCY_ELEMENT_XPATH_MAP = {
    groupId: '//project/groupId',
    parentGroupId: '//project/parent/groupId',
    artifactId: '//project/artifactId',
    description: '//project/description',
    version: '//project/version',
    parentVersion: '//project/parent/version',
  }

  DEPENDENCY_ELEMENT_XML_TEMPLATE = <<~EOS
    <dependency>
      <groupId><%= @groupId %></groupId>
      <artifactId><%= @artifactId %></artifactId>
      <version><%= @version %></version>
      <% if @description %><description><%= @description %></description><% end %>
    </dependency>
  EOS

end
