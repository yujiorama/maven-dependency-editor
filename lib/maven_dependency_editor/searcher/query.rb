module MavenDependencyEditor

  module Searcher
    class Query

      attr_accessor :strict, :source, :cache

      def initialize(h)
        @strict = false
        @source = {
          groupId: h[:groupId] || '',
          artifactId: h[:artifactId] || '',
          version: h[:version] || '',
          keyword: h[:keyword] || '',
        }
        @cache = {}
      end

      def self.of(gradleLikeDependencyDescription)
        groupId, artifactId, version = gradleLikeDependencyDescription.split(/:/)
        query = Query.new({
          groupId: groupId,
          artifactId: artifactId,
          version: version,
          keyword: nil,
        })
        query.strict = true
        query
      end

      def match?(target)
        identifiable = [:groupId, :artifactId, :keyword]
          .select { |key| ! source[key].empty? }
          .map { |key|
            if cache[key]
              cache[key]
            else
              cache[key] = ::Regexp.new(
                strict ? source[key] : ".*#{source[key]}.*",
                ::Regexp::IGNORECASE
              )
            end
          }
          .any? { |re| re.match(target) }

        if identifiable
          re = cache[:version]
          re ||= ::Regexp.new(strict ? source[:version] : ".*#{source[:version]}.*", ::Regexp::IGNORECASE)
          re.match(target)
        else
          false
        end
      end

      def to_h
        source
      end
    end
  end
end
