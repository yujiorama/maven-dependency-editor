require 'rexml/document'

module MavenDependencyEditor
  module Searcher
    module PomParser

      def self.parse(pom_file)

        unless ::File.exists?(pom_file)
          return {}
        end

        begin
          content = ::IO.read(pom_file, encoding: 'utf-8')
          document = ::REXML::Document.new(content)

          if document.nil?
            return {}
          end

          return {
            meta: {
              path: pom_file,
              mode: :local
            },
            content: document
          }

        rescue IOError => e
          return {}
        rescue ::REXML::UndefinedNamespaceException => e
          return {}
        end
      end

      def self.to_hash(document, meta)
        pom = DEPENDENCY_ELEMENT_XPATH_MAP.map { |key, xpath|
          element = ::REXML::XPath.first(document, xpath)
          value = ((element.nil? ? nil : element.text) || '')
          [key, value.gsub(/\r?\n/, ' ')]
        }.to_h

        pom[:groupId] = pom[:parentGroupId] if (pom[:groupId] || '').empty?
        pom[:version] = pom[:parentVersion] if (pom[:version] || '').empty?
        pom[:meta] = meta

        pom
      end

    end
  end
end
