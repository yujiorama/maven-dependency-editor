require 'maven_dependency_editor/searcher/pom_parser'

module MavenDependencyEditor
  module Searcher
    module Local

      def self.search(query:, format: :maven, profile: false)
        m2_repository = ENV['M2_HOME']
        if (m2_repository || '').empty?
          puts("environment variables M2_HOME is not defined")
          m2_repository = ::File.join(ENV['HOME'], '.m2', 'repository')
        end
        m2_repository_path = ::File.expand_path m2_repository

        profiler = Profiler.new(profile)

        pom_files = profiler.start(:traverse) { |stopwatch|
          ::Dir.glob(::File.join('**', '*.pom'), base: m2_repository_path)
            .map { |file| ::File.join(m2_repository_path, file) }
            .map { |file| PomParser.parse(file) }
            .select { |pom| ! pom.empty? }
          }

        found = profiler.start(:search) { |stopwatch|
          pom_files
            .map { |pom| PomParser.to_hash(pom[:content], pom[:meta]) }
            .select { |pom|
              [:groupId, :artifactId, :description].any? { |key| query.match?(pom[key]) }
            }.map { |pom|
              Renderer.new(pom, format)
            }.sort { |a, b|
              (a.groupId <=> b.groupId).nonzero? ||
              (a.artifactId <=> b.artifactId).nonzero? ||
              (a.version <=> b.version)
            }
          }

        return {
          query: query.to_h,
          results: found.map(&:to_s),
          profile: {
            total: profiler[:total],
            traverse: profiler[:traverse],
            search: profiler[:search],
          }
        }
      end
    end
  end
end