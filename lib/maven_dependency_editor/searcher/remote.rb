require 'json'
require 'net/http'
require 'uri'

module MavenDependencyEditor
  module Searcher
    module Remote

      def self.fetch(uri_str, limit:10)
        raise ArgumentError, 'HTTP redirect too deep' if limit == 0
        response = ::Net::HTTP.get_response(::URI.parse(uri_str))
        case response
        when ::Net::HTTPSuccess
          response
        when ::Net::HTTPRedirection
          fetch(response['location'], limit - 1)
        else
          response.value
        end
      end

      def self.search(query:, format: :maven, profile: false)

        profiler = Profiler.new(profile)

        page_size = 20
        search_maven_url = ::URI.parse('https://search.maven.org/solrsearch/select')

        query_string = query.to_h.values
          .reject { |v| (v || '').empty? }
          .reject { |v| v.match /\*/}
          .join(' AND ')

        found = profiler.start(:search) { |stopwatch|

          result = []
          offset = 0

          while true do
            search_maven_url.query = ::URI.encode_www_form({
              q: query_string,
              start: offset,
              rows: page_size
            })

            response = fetch(search_maven_url.to_s)
            body = ::JSON.load(response.body)
            offset = offset + page_size
            docs = body['response']['docs'].map { |v| {
                groupId: v['g'],
                artifactId: v['a'],
                version: v['latestVersion'],
                description: v['repositoryId'] + ' ' + v['text'].join(','),
                meta: {
                  repositoryId: v['repositoryId'],
                  mode: :remote
                }
              }}
              .map { |pom| Renderer.new(pom, format) }
            result = result.concat(docs)

            num_found = body['response']['numFound']
            break if offset >= num_found
          end

          result
        }

        return {
          query: query.to_h,
          results: found.map(&:to_s),
          profile: {
            total: profiler[:total],
            traverse: profiler[:traverse],
            search: profiler[:search],
          }
        }
      end
    end
  end
end
